<?php
header("Content-Type: application/json");

include_once("conexion.php");

function paises(){
	$enlace = conexion();
	mysqli_set_charset($enlace, 'utf8');

    $result =  mysqli_query($enlace,"SELECT * FROM country");

    while ($fila = mysqli_fetch_array($result)) {
        $lista[] = $fila;
		
    }
    return $lista;
}

$resultado = paises();

echo json_encode( $resultado, JSON_UNESCAPED_UNICODE );

?>