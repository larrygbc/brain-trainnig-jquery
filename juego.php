<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  
  <link rel="stylesheet" href="Css/Style.css">

  <!-- Material Design for Bootstrap fonts and icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">

  <!-- Material Design for Bootstrap CSS -->
  <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">

  <title>index</title>

</head>

<body>
  <?php 
  SESSION_START(); 
  if (isset($_SESSION['username'])) {
    ?>
    <div class="bmd-layout-container bmd-drawer-f-l bmd-drawer-overlay">
      <header class="bmd-layout-header color">
        <div class="navbar navbar-light bg-faded">
          <button class="navbar-toggler" type="button" data-toggle="drawer" data-target="#dw-s2" title="Menu">
            <span class="sr-only">Toggle drawer</span>
            <i class="material-icons text-white">menu</i>
          </button>
          
            <h2 class="text-white titulo text-uppercase pt-2 mx-auto"><a href="http://m6.test/UF4/Projecto/juego.php" class="titulo-a"><img src="Imagen/mini-logo.png" alt=""> Brain training</a></h2>
          
     <!--  <ul class="nav navbar-nav">
        <li class="nav-item">Title</li>
      </ul> -->
    </div>
  </header>
  <div id="dw-s2" class="bmd-layout-drawer bg-faded">
    <div class="card heigth">
      <div class="cover">
        <img src="Imagen/paisaje1.jpg" alt="" class="img-fluid">
      </div>
      <div class="user">
        <img src="Imagen/persona.png" alt="" class="foto-perfil">
      </div>
      <div class="main mt-5">
        <h1 id="IdLogin" class="mt-4 text-center"><?php echo $_SESSION['id']; ?></h1>
        <h1 id="UsuarioLogin" class="mt-4 text-center"><?php echo $_SESSION['username']; ?></h1>
        <div class="col-12 mt-5 d-flex flex-row">
          <a class="d-flex align-items-center link administrador"><i class="material-icons">settings</i><span class="ml-3"> Administrador</span></a>
        </div>
        <div class="col-12 d-flex flex-row mt-2">
          <a class="d-flex align-items-center link entrenar"><i class="material-icons">fitness_center</i><span class="ml-3"> Entrenar</span></a>
        </div>
        <div class="col-12 d-flex flex-row mt-2">
          <a class="d-flex align-items-center link" id="fin_sesion"><i class="material-icons">close</i><span class="ml-3">Cerrar sesion</span></a>
        </div>
      </div>
    </div>
    <div class="footer">
      <div class="rating">
      </div>
    </div>
  </div>
  <main class="bmd-layout-content">
    <div class="container" >
      <div class="row menu-mt" id='contenedor'>
        <div class="col-6 text-center">
          <a class="link entrenar"><i class="material-icons cont-icons">fitness_center</i><h6>Entrenar</h6></a>
        </div>
        <div class="col-6 text-center">
          <a class="link administrador"><i class="material-icons cont-icons">settings</i><h6>Administrador</h6></a>
        </div>
      </div>
    </div>
  </main>
</div>
<?php 
}
else{
  header("Location: index.php");
}
?>

</body>
<script src="https://code.jquery.com/jquery-3.1.0.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30="
crossorigin="anonymous"></script>

<script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>

<script src="https://cdn.rawgit.com/FezVrasta/snackbarjs/1.1.0/dist/snackbar.min.js"></script>

<script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>

<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>

<script src="js/juego.js"></script>
</html>