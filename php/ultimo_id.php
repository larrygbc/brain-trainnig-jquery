<?php
header("Content-Type: application/json");

include_once("ConexionUsuario.php");

function Ultimo_id(){
	$enlace = conexion();
	mysqli_set_charset($enlace, 'utf8');

    $result =  mysqli_query($enlace, "SELECT id FROM pregunta ORDER BY id DESC LIMIT 1");

    while ($fila = mysqli_fetch_array($result)) {
        $lista[] = $fila;
    }
    return $lista;
}

$resultado = Ultimo_id();

echo json_encode( $resultado, JSON_UNESCAPED_UNICODE );

?>