$(function() {
    //mostrar modal login
    $('#login').click(function(event) {
    	$('#myModal-login').modal('show');

    });
    $('#registrar').click(function(event) {
    	$('#myModal-registrar').modal('show');
    	$('#ErrorUsuario').hide();
    	$('#ErrorPass').hide();
    	$('#ErrorEmail').hide();
    	$('#ErrorFechaNac').hide();
    	Paises();
    });
    $('#PaisSelect').change(function(event) {
    	var ciudad = $('#PaisSelect').val();
    	Ciudades(ciudad);
    });

    // consultar usuario depues del evento
    $('#usuario').focusout(function(event) {
    	UserName();
    });

    // consultar email despues del evento
    $('#Email').focusout(function(event) {
    	Email();
    });

    $('#Aceptar').click(function() {
        var ok = true;

        //validar usuario
        if ($('#usuario').val().length <= 5) {
            ok=false;
            $('#ErrorUsuario').show(function() {
              $('#ErrorUsuario').text('Error, el campo usuario debe tener minimo 6 caracteres.');
          });
        } else {
            ok = true;
        	$('#ErrorUsuario').hide();
        }
        // validar password
        if ($('#password').val().length <= 5) {
            ok=false;
            var ExpPass = /[a-z][A-Z]{1,}[0-9]{1,}/;
            var validarPass = $('#password').val().match(ExpPass);
            if (!validarPass) {
              $('#ErrorPass').show(function() {
                 $(this).text('Error, la password debe tener al menos 6 caracteres.')
             });
          }
      } else {
        ok = true;
       $('#ErrorPass').hide();
   }

        //validar email
        var ExpEmail = /^\w{2,}[\.-]{0,}[\@]{1}\w{2,}[\.]{1}\w{2,}$/gi;
        var validar = $('#Email').val().match(ExpEmail);
        if (!validar) {
            ok=false;
            $('#ErrorEmail').show(function() {
              $(this).text('Error, el email es incorrecto.')
          });
        } else {
            ok = true;
        	$('#ErrorEmail').hide();
        }

        //validar fecha de nacimiento
        var fecha = $('#Fech_nac').val();
        
        if (fecha.length == 0) {
            ok=false;
        	$('#ErrorFechaNac').show(function() {
        		$(this).text('Error, tienes que introducir una fecha.');
        	});
        }else{
            ok=true;
        }
        var año = fecha.substring(0, fecha.indexOf("-"));
        var mes = fecha.substr(fecha.indexOf("-") + 1, fecha.indexOf("-") - 2);
        var dia = fecha.substring(fecha.length - 2, fecha.length);

        var fecha_nac = new Date(año, mes - 1, dia);

        var fecha_actual = new Date();
        var años = fecha_actual - fecha_nac;
        años = Math.floor(años / (1000 * 60 * 60 * 24 * 365));
        if (años < 16) {
            ok=false;
            $('#ErrorFechaNac').show(function() {
              $(this).text('Error, tienes que al menos tener 16 años.');
          });

        } else {
            ok=true;
        	$('#ErrorFechaNac').hide();
        }
        if (ok) {
            $.ajax({
                url: 'php/registrar.php',
                dataType: 'json',
                data: {
                    u: $('#usuario').val(),
                    p: $('#password').val(),
                    e: $('#Email').val(),
                    f: $('#Fech_nac').val(),
                },
            })
            .done(function() {
                $.snackbar({content: "Se ha registrado correctamente!"});
            })    
        };

        
        
    });

    $('#AceptarId').click(function(event) {

    	var user = $('#LoginUser').val();
    	var pass = $('#LoginPassword').val();

    	Login(user, pass);
    });
});

//mostrar paises
var Paises = function() {
	$('#PaisSelect').html();
	$.ajax({
		url: 'php/ConsultarPais.php',
		type: 'GET',
		dataType: 'json',
	})
	.done(function(response) {
		var resp = response;
		for (var i = 0; i < resp.length; i++) {
			var x = "<option value='" + resp[i].Code + "'>" + resp[i].Name + "</option>";
			$('#PaisSelect').append(x);
		}
	});
}

//comprobar que el usuario existe
var UserName = function() {
	var usuario = $('#usuario').val();
	$.ajax({
		url: 'php/usuario.php',
		dataType: 'json',
		data: {
			user: usuario
		},
	})
	.done(function(response) {
		if (response.length > 0) {
			$('#ErrorUsuario').show(function() {
				$(this).text('Error, este usuario existe.');
			});
		} else {
			$('#ErrorUsuario').hide();
		}
	})
}

//comprobar si el email ya existe
var Email = function() {
	var email = $('#Email').val();
	$.ajax({
		url: 'php/ComprobarEmail.php',
		dataType: 'json',
		data: {
			mail: email
		},
	})
	.done(function() {
		$('#ErrorEmail').show(function() {
			$(this).text('Error, este email ya existe.');
		});
	})
}

var Login = function(user, pass) {
    //usuario
    $.ajax({
    	url: 'php/LoginUser.php',
    	dataType: 'json',
    	data: {
    		user: user,
    		pass: pass
    	},
    })
    .done(function() {
    	usuario = true;
    	$('#ErrorLogin').hide();
    })
    .fail(function() {
    	$('#ErrorLogin').show(function() {
    		$('#ErrorLogin').text('Error, el usuario no existe.');
    	});
    })

    //password
    $.ajax({
    	url: 'php/LoginPass.php',
    	dataType: 'json',
    	data: {
    		user: user,
    		pass: pass
    	},
    })
    .done(function(response) {
    	$('#ErrorLoginPass').hide();
    	window.location.href = "http://m6.test/UF4/Projecto/juego.php";
    })
    .fail(function() {
    	$('#ErrorLoginPass').show(function() {
    		$('#ErrorLoginPass').text('Error, la contraseña no es correcta.');
    	});
    	$('#myModal-login').effect('shake');
    })
}

//mostrar ciudades
var Ciudades = function(valor) {
	$.ajax({
		url: 'php/ConsultarCiudades.php',
		dataType: 'json',
		data: {
			ciudad: valor
		},
	})
	.done(function(response) {
		$('#CiudadSelect').text('');
		var resp = response;
		for (var i = 0; i < resp.length; i++) {
			var x = "<option value='" + resp[i].Name + "'>" + resp[i].Name + "</option>";
			$('#CiudadSelect').append(x);
		}
	});
}