<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  
  <link rel="stylesheet" href="Css/Style.css">

  <!-- Material Design for Bootstrap fonts and icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">

  <!-- Material Design for Bootstrap CSS -->
  <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">

  <script src="https://code.jquery.com/jquery-3.1.0.js"></script>

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
  integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30="
  crossorigin="anonymous"></script>

  <script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>

  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500i,700" rel="stylesheet">

  <script src="https://cdn.rawgit.com/FezVrasta/snackbarjs/1.1.0/dist/snackbar.min.js"></script>

  <script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>
  <script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>

  <script src="js/script.js"></script>

  <title>index</title>
</head>

<body>
 <div class="bmd-layout-container bmd-drawer-f-l bmd-drawer-overlay">
  <header class="bmd-layout-header color">
    <div class="navbar navbar-light bg-faded">
      <button class="navbar-toggler float-right" type="button" id="login" title="Iniciar sesión">
        <span class="sr-only">Toggle drawer</span>
        <i class="material-icons text-white">person</i>
      </button>

      <h2 class="text-white titulo text-uppercase pt-2"><img src="Imagen/mini-logo.png" alt=""> Brain training</h2>
      <button class="navbar-toggler float-right" type="button" id="registrar" title="Registrar">
        <span class="sr-only">Toggle drawer</span>
        <i class="material-icons text-white">person_add</i>
      </button>

     <!--  <ul class="nav navbar-nav">
        <li class="nav-item">Title</li>
      </ul> -->
    </div>
  </header>
  
  <main class="bmd-layout-content">
    <div class="container-fluid heigth">
      <div class="row">
        <img src="Imagen/logo.png" alt="Brain Trainnig" class="img-fluid mx-auto mt-5 mb-5">
      </div>
    </div>
  </main>
</div>

<!-- Modal registrar -->
<div class="modal fade" id="myModal-registrar">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalTitle">Registrar usuario</h5>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="User">Usuario</label>
            <input type="text" class="form-control" id="usuario" placeholder="Enter user">
            <span class="error" id="ErrorUsuario"></span>
          </div>
          <div class="form-group">
            <label for="Password">Password</label>
            <input type="password" class="form-control" id="password" placeholder="Password">
            <span class="error" id="ErrorPass"></span>
          </div>
          <div class="form-group">
            <label for="Email">Email</label>
            <input type="email" class="form-control" id ="Email" aria-describedby="emailHelp" placeholder="Enter email">
            <span class="error" id="ErrorEmail"></span>
          </div>
          <div class="form-group">
            <label for="date">Fecha de nacimiento</label>
            <input type="date" class="form-control" id="Fech_nac">
            <span class="error" id="ErrorFechaNac"></span>
          </div>
          <div class="form-group">
            <label for="">Pais</label>
            <select class="custom-select" required id="PaisSelect">
              <option value="" class="mt-3"></option>
            </select>
            <div class="invalid-feedback">Example invalid custom select feedback</div>
          </div>
          <div class="form-group">
            <label for="">Poblacion</label>
            <select class="custom-select" required id="CiudadSelect">
              <option value=""></option>
            </select>
            <div class="invalid-feedback">Example invalid custom select feedback</div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-raised btn-color"  data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-raised btn-color ml-2" id="Aceptar">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal login -->
<div class="modal fade" id="myModal-login">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Iniciar sesión</h5>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="exampleInputEmail1">Email o usuario</label>
            <input type="email" class="form-control" id="LoginUser" aria-describedby="emailHelp" placeholder="Enter email o usuario">
            <span class="error" id="ErrorLogin"></span>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="LoginPassword" placeholder="Password">
            <span class="error" id="ErrorLoginPass"></span>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-raised btn-color" data-dismiss="modal" >Cerrar</button>
        <button type="button" class="btn btn-raised btn-color ml-2" id="AceptarId" >Aceptar</button>
      </div>
    </div>
  </div>
</div>
</body>
</html>