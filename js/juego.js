
var Stop;
var enunciados;
var num;
var Num_preguntas = [];
var respuesta = [];
Respuesta_correcta = 0;

$(function(){
	$('.entrenar').click(function(event) {
		$('#contenedor').empty();
		Ultimo();
		
	});

	$('.administrador').click(function(event) {
		$('#contenedor').empty();
		$('#contenedor').load('http://m6.test/UF4/Projecto/html/menu_preguntas.html',function(){

			//Crear preguntas
			$('#crear').click(function(event) {
				$('#contenedor').empty();
				$('#contenedor').removeClass('menu-mt');
				$('#contenedor').load('http://m6.test/UF4/Projecto/html/form_ins.html', function() {
					$('#añadir').click(function(event) {
						var e = $('#enunciado').val();
						var r1 = $('#res_1').val();
						var r2 = $('#res_2').val();
						var r3 = $('#res_3').val();
						var r4 = $('#res_4').val();
						var rc = $('#res_c').val();
						var c = $("#select").val();

						$('#form').submit(function(event) {
							return false;
						});
						InsertarPregunta(e,r1,r2,r3,r4,rc,c);

					});
				});		
			});

			//Modificar las preguntas
			$('#modificar').click(function(event) {
				$('#contenedor').empty();
				$('#contenedor').removeClass('menu-mt');
				var tabla = "<h1 class='mx-auto m-5'>Modificar Preguntas</h1>" +
				"<table class='mx-auto text-center table mb-5' id='tabla' >" +
				"<thead>" +
				"<tr>" +
				"<th scope='col'>Id</th>" +
				"<th scope='col'>Enunciado</th>" +
				"<th scope='col'>Respuesta 1</th>" +
				"<th scope='col'>Respuesta 2</th>" +
				"<th scope='col'>Respuesta 3</th>" +
				"<th scope='col'>Respuesta 4</th>" +
				"<th scope='col'>Respuesta Correcta</th>" +
				"<th scope='col'>Categoria</th>" +
				"<th scope='col'>Modificar</th>" +
				"</tr>" +
				"</thead>" +
				"<tbody id='tbody'></tbody>" +
				"</table>";

				$('#contenedor').append(tabla);

				ModificarPreguntas();
			});

			//Eliminar pregunta
			$('#eliminar').click(function(event) {
				$('#contenedor').empty();
				$('#contenedor').removeClass('menu-mt');
				var tabla = "<h1 class='mx-auto m-5'>Eliminar Preguntas</h1>" +
				"<table class='mx-auto text-center table mb-5' id='tabla' >" +
				"<thead>" +
				"<tr>" +
				"<th scope='col'>Id</th>" +
				"<th scope='col'>Enunciado</th>" +
				"<th scope='col'>Respuesta 1</th>" +
				"<th scope='col'>Respuesta 2</th>" +
				"<th scope='col'>Respuesta 3</th>" +
				"<th scope='col'>Respuesta 4</th>" +
				"<th scope='col'>Respuesta Correcta</th>" +
				"<th scope='col'>Categoria</th>" +
				"<th scope='col'>Eliminar</th>" +
				"</tr>" +
				"</thead>" +
				"<tbody id='tbody'></tbody>" +
				"</table>";

				$('#contenedor').append(tabla);

				EliminarPregunta();
			});
		});
	});

	$('#fin_sesion').click(function(event) {
		$.ajax({
			url: 'php/Cerrar_sesion.php',
			dataType: 'json',
		})
		.always(function() {
			window.location.href = "http://m6.test/UF4/Projecto/index.php";
		})
	});	
})

//funcion para crear preguntas
function InsertarPregunta(e,r1,r2,r3,r4,rc,c){
	$.ajax({
		url: 'php/insertar_pregunta.php',
		dataType: 'json',
		data: {
			e: e,
			r1: r1,
			r2: r2,
			r3: r3,
			r4: r4,
			rc: rc,
			c: c,
		},
	})
	.done(function(resp) {
		$.snackbar({content: "Se ha añadido la pregunta correctamente!"});
		setTimeout(function() {
			window.location.href = "http://m6.test/UF4/Projecto/juego.php";
		}, 3300);
	})
}

//funcion para modificar una pregunta
function ModificarPreguntas(){
	var id;

	$.ajax({
		url: 'php/Modificar_preguntas.php',
		dataType: 'json',
	})
	.done(function(resp) {

		for (var i = 0; i < resp.length; i++) {
			var tr = "<tr>" + 
			"<th scope='row'>" + resp[i].id + "</th>" +
			"<td>" + resp[i].enunciado + "</td>" +
			"<td>" + resp[i].respuesta1 + "</td>" +
			"<td>" + resp[i].respuesta2 + "</td>" +
			"<td>" + resp[i].respuesta3 + "</td>" +
			"<td>" + resp[i].respuesta4 + "</td>" +
			"<td>" + resp[i].resp_correcta + "</td>" +
			"<td>" + resp[i].id_tema + "</td>" +
			"<td><a class='link modificar' id='modificar"+ resp[i].id  + "'><i class='material-icons'>mode_edit</i></a></td>" +
			"</tr>";

			$('#tbody').append(tr);
		};
	})
	.always(function(resp){
		
		$('.modificar').click(function(event) {
			var id_sub = $(this).attr('id');
			id = id_sub.substr(id_sub.indexOf("r")+1);
			$('#contenedor').append("<div id='modal'></div>");
			$('#modal').load('http://m6.test/UF4/Projecto/html/modal_modificar.html', function() {
				
				$('#InputEnunciado').val(resp[id].enunciado);

				$('#InputRespuesta1').val(resp[id].respuesta1);
				$('#InputRespuesta2').val(resp[id].respuesta2);
				$('#InputRespuesta3').val(resp[id].respuesta3);
				$('#InputRespuesta4').val(resp[id].respuesta4);
				$('#InputRespuestaCorrecta').val(resp[id].resp_correcta);
				if (resp[id].id_tema == 1) {
					$("#select option[value='Mat']").attr('selected', true);
				}else if (resp[id].id_tema ==2) {
					$("#select option[value='His']").attr('selected', true);
				}else if (resp[id].id_tema ==3){
					$("#select option[value='CG']").attr('selected', true);
				}
				$('#modal_mod').modal('show');
				$('#modificar_datos').click(function(event) {
					var e = $('#InputEnunciado').val();
					var r1 = $('#InputRespuesta1').val();
					var r2 = $('#InputRespuesta2').val();
					var r3 = $('#InputRespuesta3').val();
					var r4 = $('#InputRespuesta4').val();
					var rc = $('#InputRespuestaCorrecta').val();
					var t = $('#select').val();

					$.ajax({
						url: 'php/actualizar_pregunta.php',
						dataType: 'json',
						data: {
							id: id,
							e: e,
							r1: r1,
							r2: r2,
							r3: r3,
							r4: r4,
							rc: rc,
							t: t,
						},
					})
					.done(function(response) {
						$.snackbar({content: "Se ha actualizado la pregunta correctamente!"});
						setTimeout(function() {
							window.location.href = "http://m6.test/UF4/Projecto/juego.php";
						}, 3300);
					})
				});
			});		
		});
	})	
}

//funcion para eliminar preguntas
function EliminarPregunta(){
	var id;
	$.ajax({
		url: 'php/Modificar_preguntas.php',
		dataType: 'json',
	})
	.done(function(resp) {

		for (var i = 0; i < resp.length; i++) {
			var tr = "<tr>" + 
			"<th scope='row'>" + resp[i].id + "</th>" +
			"<td>" + resp[i].enunciado + "</td>" +
			"<td>" + resp[i].respuesta1 + "</td>" +
			"<td>" + resp[i].respuesta2 + "</td>" +
			"<td>" + resp[i].respuesta3 + "</td>" +
			"<td>" + resp[i].respuesta4 + "</td>" +
			"<td>" + resp[i].resp_correcta + "</td>" +
			"<td>" + resp[i].id_tema + "</td>" +
			"<td><a class='link Eliminar' id='Eliminar"+ resp[i].id  + "'><i class='material-icons'>delete</i></a></td>" +
			"</tr>";

			$('#tbody').append(tr);			
		};
	})
	.always(function(){
		$('.Eliminar').click(function(event) {
			var id_sub = $(this).attr('id');
			id = id_sub.substr(id_sub.indexOf("r")+1);
			$.ajax({
				url: 'php/eliminar_pregunta.php',
				dataType: 'json',
				data: {
					id: id,
				},
			})
			.done(function() {
				$.snackbar({content: "Se ha eliminado la pregunta correctamente!"});
				setTimeout(function() {
					window.location.href = "http://m6.test/UF4/Projecto/juego.php";
				}, 3300);
			})
		});	
	})
}


var Ultimo = function(){

	//array con 10 numeros aleatorios
	var cantidadNumeros = 10;
	Num_preguntas = []
	while(Num_preguntas.length < cantidadNumeros ){
		var numeroAleatorio = Math.ceil(Math.random()*16);
		var existe = false;
		for(var i=0;i<Num_preguntas.length;i++){
			if(Num_preguntas [i] == numeroAleatorio){
				existe = true;
				break;
			}
		}
		if(!existe){
			Num_preguntas[Num_preguntas.length] = numeroAleatorio;
		}
	}

	$.ajax({
		url: 'php/pregunta.php',
		dataType: 'json',
		data: {
			id: Num_preguntas
		},
	})
	.done(function(response) {
		enunciados = response;
		num = 0;

		mostrar(num);

	})
}

function mostrar(i){
	
	if(num < 10){

		$('#contenedor').empty();

		var enunciado =	"<div class='col-12 mb-4' id='pregunta" + i + "'>" + 
		"<div class='row'>" +
		"<div class='col-12 text-center mt-4 text-pregunta'>" + enunciados[i].enunciado + "</div>" + 
		"<div class='offset-3 col-3 text-center mt-5 p-3 color1 text-respuesta respuesta'><a class='link text-white' id='respuesta1'>" + enunciados[i].respuesta1 + "</a></div>" + 
		"<div class='col-3 text-center mt-5 p-3 color2 text-respuesta respuesta'><a class='link text-white' id='respuesta2'>" + enunciados[i].respuesta2 + "</a></div>" +
		"<div class='offset-3 col-3 text-center p-3 color3 text-respuesta respuesta'><a class='link text-white' id='respuesta3'>" + enunciados[i].respuesta3 + "</a></div>" +
		"<div class='col-3 text-center p-3 color4 text-respuesta respuesta'><a class='link text-white' id='respuesta4'>" + enunciados[i].respuesta4 + "</a></div></div></div>";

		$('#contenedor').append(enunciado);

		
		respuesta.push('No respondido');

		$('.respuesta a').click(function(event) {
			respuesta.pop();
			respuesta.push($(this).text());
			console.log(respuesta);
			clearTimeout(Stop);
			mostrar(++num);
		});

		Stop = setTimeout(function(){ 
			mostrar(++num);
		}, 3000);
	}else{
		$('#contenedor').empty();
		resultado();
	}
}

function resultado(){
	$.ajax({
		url: 'php/Respuestas.php',
		dataType: 'json',
		data: {
			id: Num_preguntas
		},
	})
	.done(function(response) {

		$('#contenedor').removeClass('menu-mt');
		$('#contenedor').html("<h1 class='mx-auto mt-5'>Tu Resultado Final</h1>");
		for (var i = 0; i < Num_preguntas.length; i++) {
			var enunciado =	"<div class='col-12 mb-4' id='pregunta" + i + "'>" + 
			"<div class='row'>" +
			"<div class='col-12 text-center mt-4 text-pregunta'>" + enunciados[i].enunciado + "</div>" + 
			"<div class='offset-3 col-3 text-center mt-5 p-3 color1 text-respuesta respuesta'>" + enunciados[i].respuesta1 + "</div>" + 
			"<div class='col-3 text-center mt-5 p-3 color2 text-respuesta respuesta'>" + enunciados[i].respuesta2 + "</div>" +
			"<div class='offset-3 col-3 text-center p-3 color3 text-respuesta respuesta'>" + enunciados[i].respuesta3 + "</div>" +
			"<div class='col-3 text-center p-3 color4 text-respuesta respuesta'>" + enunciados[i].respuesta4 + "</div></div></div>";
			

			$('#contenedor').append(enunciado);

			if (response[i].resp_correcta == respuesta[i]) {
				var correcto = 
				"<div class='d-flex align-items-center justify-content-center offset-3 col-6 mt-2 bg-success text-center'>" +
				"<i class='material-icons mr-3'>check</i><p class='mt-3'> Respuesta correcta</p>" +
				"</div>";

				$('#contenedor').append(correcto);

				Respuesta_correcta++;

			};
		};

		var random = Math.floor(Math.random()* 10);
		var result = (100 - (Respuesta_correcta * 10) + random);

		$('#contenedor').append("<h4 class='d-flex align-items-center bg-info p-4 mt-4 mx-auto'><i class='material-icons mr-3'>face</i> Tu edad mental es de " + result + " </h4>");

		var fecha = new Date();
		var fecha_actual = fecha.getFullYear() + '-' + (fecha.getMonth()+1) + '-' + fecha.getDate();
		console.log(fecha_actual);
		console.log($('#IdLogin').text());

		$.ajax({
			url: 'php/Insertar_edad.php',
			dataType: 'json',
			data: {
				id_usuario: $('#IdLogin').text(),
				edad: result, 
				fecha: fecha_actual,
			},
		})
		.done(function() {
			$.snackbar({content: "Se ha guardado la partida correctamente!"});
		})
		
	})
	
}
